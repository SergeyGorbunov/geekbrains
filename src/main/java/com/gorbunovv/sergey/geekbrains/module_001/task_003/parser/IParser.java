package com.gorbunovv.sergey.geekbrains.module_001.task_003.parser;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 10/6/2021
 * Time: 6:40 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public interface IParser {
    List<String> expression2List(String expression);
}
