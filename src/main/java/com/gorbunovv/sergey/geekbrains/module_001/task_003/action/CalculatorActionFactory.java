package com.gorbunovv.sergey.geekbrains.module_001.task_003.action;

import com.gorbunovv.sergey.geekbrains.module_001.task_003.action.impl.*;

import java.util.Stack;

import static com.gorbunovv.sergey.geekbrains.module_001.task_003.action.CalculatorActionType.*;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 10/6/2021
 * Time: 5:36 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public class CalculatorActionFactory {
    public static ICalculatorAction<Double> createAction(Stack<Double> stack, String s) {
        if (s.equals(PLUS.getType())) return new PlusCalculatorAction(s, stack);
        if (s.equals(MINUS.getType())) return new MinusCalculatorAction(s, stack);
        if (s.equals(MULTIPLICATION.getType())) return new MultiplicationCalculatorAction(s, stack);
        if (s.equals(DIVISION.getType())) return new DivisionCalculatorAction(s, stack);
        if (s.equals(UNARY_MINUS.getType())) return new UnaryMinusCalculatorAction(s, stack);
        if (s.equals(UNARY_PLUS.getType())) return new UnaryPlusCalculatorAction(s, stack);
        return new SimplePushCalculatorAction(s, stack);
    }
}
