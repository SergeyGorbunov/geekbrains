package com.gorbunovv.sergey.geekbrains.module_001.task_002.calculator;

import com.gorbunovv.sergey.geekbrains.module_001.task_002.calculator.impl.CalculatorImpl;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 9/26/2021
 * Time: 1:12 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public class Calculators {
    public static ICalculator create() {
        return new CalculatorImpl();
    }
}
