package com.gorbunovv.sergey.geekbrains.module_001.task_003.action;

import java.util.Stack;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 10/6/2021
 * Time: 5:29 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public interface ICalculatorAction<T> {
    Stack<T> doAction();
}
