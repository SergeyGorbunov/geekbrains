package com.gorbunovv.sergey.geekbrains.module_001.task_002.calculator.impl;

import com.gorbunovv.sergey.geekbrains.module_001.task_001.Calculator;
import com.gorbunovv.sergey.geekbrains.module_001.task_002.calculator.ICalculator;
import com.gorbunovv.sergey.geekbrains.module_001.task_002.expression.parser.ExpressionParsers;
import com.gorbunovv.sergey.geekbrains.module_001.task_002.expression.parser.IExpressionParser;
import com.gorbunovv.sergey.geekbrains.module_001.task_002.expression.polish.ExpressionPolishes;
import com.gorbunovv.sergey.geekbrains.module_001.task_002.expression.polish.IExpressionPolish;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 9/26/2021
 * Time: 1:02 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public class CalculatorImpl implements ICalculator {

    @Override
    public Double calculateWithDebug(String expression) {
        System.out.println("EXPRESSION = " + expression);

        List<String> lexemes = ExpressionParsers.create().expression2Lexemes(expression);
        System.out.println("LEXEMES = " + lexemes);

        List<String> reversePolishNotation = ExpressionPolishes.create().lexemes2ReversePolishNotation(lexemes);
        System.out.println("REVERSE POLISH NOTATION = " + reversePolishNotation);

        return Calculator.calculate(reversePolishNotation);
    }

    @Override
    public Double calculate(String expression) {
        return calculate(
                expression,
                ExpressionParsers.create(),
                ExpressionPolishes.create());
    }

    @Override
    public Double calculate(String expression, IExpressionParser expressionParser) {
        return calculate(
                expression,
                expressionParser,
                ExpressionPolishes.create());
    }

    @Override
    public Double calculate(String expression, IExpressionParser expressionParser, IExpressionPolish expressionPolish) {
        return Calculator.calculate(expressionPolish.lexemes2ReversePolishNotation(expressionParser.expression2Lexemes(expression)));
    }
}
