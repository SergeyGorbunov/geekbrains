package com.gorbunovv.sergey.geekbrains.module_001.task_002.expression.polish;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 9/26/2021
 * Time: 12:10 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public interface IExpressionPolish {
    List<String> lexemes2ReversePolishNotation(List<String> lexemes);
}
