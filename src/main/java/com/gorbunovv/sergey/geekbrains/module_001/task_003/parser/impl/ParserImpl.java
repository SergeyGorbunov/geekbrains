package com.gorbunovv.sergey.geekbrains.module_001.task_003.parser.impl;

import com.gorbunovv.sergey.geekbrains.module_001.task_003.parser.IParser;

import java.util.Arrays;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 10/6/2021
 * Time: 6:42 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public class ParserImpl implements IParser {

    @Override
    public List<String> expression2List(String expression) {
        return Arrays.asList(expression.split(" "));
    }
}
