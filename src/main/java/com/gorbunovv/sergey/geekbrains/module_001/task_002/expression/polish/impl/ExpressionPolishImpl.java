package com.gorbunovv.sergey.geekbrains.module_001.task_002.expression.polish.impl;

import com.gorbunovv.sergey.geekbrains.module_001.task_001.Calculator;
import com.gorbunovv.sergey.geekbrains.module_001.task_002.expression.polish.IExpressionPolish;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 9/26/2021
 * Time: 1:01 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public class ExpressionPolishImpl implements IExpressionPolish {
    @Override
    public List<String> lexemes2ReversePolishNotation(List<String> lexemes) {
        return Calculator.getReversePolishNotation(lexemes);
    }
}
