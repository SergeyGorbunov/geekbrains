package com.gorbunovv.sergey.geekbrains.module_001.task_002.expression.parser;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 9/26/2021
 * Time: 1:48 AM
 * To change this template use File | Settings | File and Code Templates.
 */
public interface IExpressionParser {
    List<String> expression2Lexemes(String expression);
}
