package com.gorbunovv.sergey.geekbrains.module_001.task_002.expression.polish;

import com.gorbunovv.sergey.geekbrains.module_001.task_002.expression.polish.impl.ExpressionPolishImpl;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 9/26/2021
 * Time: 1:13 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public class ExpressionPolishes {
    public static IExpressionPolish create() {
        return new ExpressionPolishImpl();
    }
}
