package com.gorbunovv.sergey.geekbrains.module_001.task_003.action;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 10/6/2021
 * Time: 5:51 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public enum CalculatorActionType {
    PLUS("+"),
    MINUS("-"),
    MULTIPLICATION("*"),
    DIVISION("/"),
    UNARY_MINUS("u-"),
    UNARY_PLUS("u+");

    private final String type;

    CalculatorActionType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
