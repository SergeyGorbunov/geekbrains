package com.gorbunovv.sergey.geekbrains.module_001.task_002.expression.parser;

import com.gorbunovv.sergey.geekbrains.module_001.task_002.expression.parser.impl.ExpressionParserImpl;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 9/26/2021
 * Time: 1:12 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public class ExpressionParsers {
    public static IExpressionParser create() {
        return new ExpressionParserImpl();
    }
}
