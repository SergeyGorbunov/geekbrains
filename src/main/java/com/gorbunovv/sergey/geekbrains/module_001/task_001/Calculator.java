package com.gorbunovv.sergey.geekbrains.module_001.task_001;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 9/24/2021
 * Time: 11:42 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public class Calculator {

    private static final List<Character> OPERATOR_CHARS = List.of('+', '-', '/', '*');
    private static final List<String> OPERATOR_STRINGS = List.of("+", "-", "/", "*");

    public static void parseAndCalculate(String expression) {
        System.out.println("EXPRESSION = " + expression);
        List<String> lexemes = parseLexemes(expression);
        System.out.println("LEXEMES = " + String.join(", ", lexemes));

        List<String> reversePolishNotation = getReversePolishNotation(lexemes);
        System.out.println("REVERSE POLISH NOTATION = " + reversePolishNotation);

        Double calculate = calculate(reversePolishNotation);
        System.out.println("CALCULATE = " + calculate);
    }

    public static List<String> parseLexemes(String expression) {
        List<String> lexemes = new ArrayList<>();
        StringBuilder builder = new StringBuilder();

        for (char c : expression.toCharArray()) {
            if (isCharOperator(c) || isCharBracket(c)) {
                addAndClear(lexemes, builder);
                lexemes.add(Character.toString(c));
            }
            if (isCharNumber(c) || isCharDot(c)) {
                builder.append(c);
            }
        }

        addAndClear(lexemes, builder);
        return lexemes;
    }

    private static void addAndClear(List<String> lexemes, StringBuilder builder) {
        if (builder.length() != 0) {
            lexemes.add(builder.toString());
            builder.setLength(0);
        }
    }

    public static List<String> getReversePolishNotation(List<String> lexemes) {
        Stack<String> stack = new Stack<>();
        List<String> result = new ArrayList<>();

        String prev = "";
        for (String curr : lexemes) {
            if (isNumber(curr)) {
                result.add(curr);
            } else {

                // (
                if (isLeftBracket(curr)) {
                    stack.push(curr);
                }

                // )
                else if (isRightBracket(curr)) {
                    while (!isLeftBracket(stack.peek())) {
                        result.add(stack.pop());
                        if (stack.isEmpty()) {
                            return result;
                        }
                    }
                    stack.pop();
                    if (!stack.isEmpty()) {
                        result.add(stack.pop());
                    }
                }

                // unary && others
                else {

                    // -
                    if (curr.equals("-") && (prev.equals("") || (!isNumber(prev) && !isRightBracket(prev)))) {
                        curr = "u-";
                    }

                    // +
                    else if (curr.equals("+") && (prev.equals("") || (!isNumber(prev) && !isRightBracket(prev)))) {
                        curr = "u+";
                    }

                    // others
                    else {
                        while (!stack.isEmpty() && (getPriority(curr) <= getPriority(stack.peek()))) {
                            result.add(stack.pop());
                        }
                    }
                    stack.push(curr);
                }
            }

            prev = curr;
        }

        while (!stack.isEmpty()) {
            if (isOperator(stack.peek())) {
                result.add(stack.pop());
            } else {
                return result;
            }
        }

        return result;
    }

    public static Double calculate(List<String> expression) {
        Stack<Double> stack = new Stack<>();

        for (String s : expression) {
            if (s.equals("+")) calcPlus(stack);
            else if (s.equals("-")) calcMinus(stack);
            else if (s.equals("*")) calcMultiplication(stack);
            else if (s.equals("/")) calcDivision(stack);
            else if (s.equals("u-")) calcUnaryMinus(stack);
            else if (s.equals("u+")) calcUnaryPlus(stack);
            else calcPush(s, stack);
        }

        return stack.pop();
    }

    public static void calcPlus(Stack<Double> stack) {
        stack.push(stack.pop() + stack.pop());
    }

    public static void calcMinus(Stack<Double> stack) {
        Double b = stack.pop();
        Double a = stack.pop();
        stack.push(a - b);
    }

    public static void calcMultiplication(Stack<Double> stack) {
        stack.push(stack.pop() * stack.pop());
    }

    public static void calcDivision(Stack<Double> stack) {
        Double b = stack.pop();
        Double a = stack.pop();
        stack.push(a / b);
    }

    public static void calcUnaryMinus(Stack<Double> stack) {
        stack.push(-stack.pop());
    }

    public static void calcUnaryPlus(Stack<Double> stack) {
        stack.push(stack.pop());
    }

    public static void calcPush(String s, Stack<Double> stack) {
        stack.push(Double.valueOf(s));
    }

    private static boolean isCharDot(char c) {
        return c == '.';
    }

    private static boolean isCharNumber(char c) {
        return c >= '0' && c <= '9';
    }

    private static boolean isCharOperator(char c) {
        return OPERATOR_CHARS.contains(c);
    }

    private static boolean isCharBracket(char c) {
        return c == '(' || c == ')';
    }

    private static boolean isNumber(String s) {
        try {
            Double.parseDouble(s);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private static boolean isOperator(String s) {
        return OPERATOR_STRINGS.contains(s);
    }

    private static boolean isLeftBracket(String s) {
        return s.equals("(");
    }

    private static boolean isRightBracket(String s) {
        return s.equals(")");
    }

    private static int getPriority(String s) {
        if (s.equals("(")) return 0;
        if (s.equals("+") || s.equals("-")) return 1;
        if (s.equals("*") || s.equals("/")) return 2;
        return 3;
    }
}
