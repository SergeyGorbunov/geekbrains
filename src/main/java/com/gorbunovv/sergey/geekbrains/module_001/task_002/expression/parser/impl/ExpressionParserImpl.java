package com.gorbunovv.sergey.geekbrains.module_001.task_002.expression.parser.impl;

import com.gorbunovv.sergey.geekbrains.module_001.task_001.Calculator;
import com.gorbunovv.sergey.geekbrains.module_001.task_002.expression.parser.IExpressionParser;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 9/26/2021
 * Time: 1:02 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public class ExpressionParserImpl implements IExpressionParser {
    @Override
    public List<String> expression2Lexemes(String expression) {
        return Calculator.parseLexemes(expression);
    }
}
