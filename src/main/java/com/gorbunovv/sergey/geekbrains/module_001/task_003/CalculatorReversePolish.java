package com.gorbunovv.sergey.geekbrains.module_001.task_003;

import com.gorbunovv.sergey.geekbrains.module_001.task_003.action.CalculatorActionFactory;
import com.gorbunovv.sergey.geekbrains.module_001.task_003.parser.Parsers;

import java.util.Stack;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 10/6/2021
 * Time: 8:07 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public class CalculatorReversePolish {
    public static Double calculate(String expression) {
        return Parsers
                .createParser()
                .expression2List(expression)
                .stream()
                .reduce(

                        // start
                        new Stack<Double>(),

                        // accumulator
                        (stack, s) -> CalculatorActionFactory
                                .createAction(stack, s)
                                .doAction(),

                        // combined
                        (stack, stack2) -> {
                            stack2.forEach(stack::push);
                            return stack2;
                        })
                .pop();
    }

}
