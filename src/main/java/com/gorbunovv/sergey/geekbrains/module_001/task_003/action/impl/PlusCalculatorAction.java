package com.gorbunovv.sergey.geekbrains.module_001.task_003.action.impl;

import com.gorbunovv.sergey.geekbrains.module_001.task_001.Calculator;
import com.gorbunovv.sergey.geekbrains.module_001.task_003.action.ICalculatorAction;

import java.util.Stack;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 10/6/2021
 * Time: 5:35 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public class PlusCalculatorAction implements ICalculatorAction<Double> {

    private String s;
    private Stack<Double> stack;

    public PlusCalculatorAction(String s, Stack<Double> stack) {
        this.s = s;
        this.stack = stack;
    }

    @Override
    public Stack<Double> doAction() {
        Calculator.calcPlus(stack);
        return (Stack<Double>) stack.clone();
    }
}
