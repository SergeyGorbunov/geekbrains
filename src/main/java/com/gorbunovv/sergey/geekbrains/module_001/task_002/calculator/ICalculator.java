package com.gorbunovv.sergey.geekbrains.module_001.task_002.calculator;

import com.gorbunovv.sergey.geekbrains.module_001.task_002.expression.parser.IExpressionParser;
import com.gorbunovv.sergey.geekbrains.module_001.task_002.expression.polish.IExpressionPolish;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 9/26/2021
 * Time: 1:48 AM
 * To change this template use File | Settings | File and Code Templates.
 */
public interface ICalculator {

    Double calculateWithDebug(String expression);

    Double calculate(String expression);

    Double calculate(String expression, IExpressionParser expressionParser);

    Double calculate(String expression, IExpressionParser expressionParser, IExpressionPolish expressionPolish);
}
